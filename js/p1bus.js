const http = new XMLHttpRequest();
const url = "https://jsonplaceholder.typicode.com/albums";
let albumsData = [];

// Función para realizar la petición y cargar los datos
function cargarDatos() {
    http.open("GET", url, true);
    http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
            albumsData = JSON.parse(http.responseText);
        }
    };
    http.send();
}

// Función para mostrar los datos en la tabla
function mostrarDatos(data) {
    const lista = document.getElementById("lista");
    lista.innerHTML = ""; // Limpiar la tabla antes de mostrar nuevos datos

    data.forEach(album => {
        const row = document.createElement("tr");
        row.innerHTML = `<td class="columna1">${album.userId}</td><td class="columna2">${album.id}</td><td class="columna3">${album.title}</td>`;
        lista.appendChild(row);
    });
}

// Función para realizar la búsqueda por ID
function buscarPorId() {
    const searchInput = document.getElementById("searchInput");
    const searchTerm = parseInt(searchInput.value);

    if (!isNaN(searchTerm) && searchTerm >= 1 && searchTerm <= 100) {
        const foundAlbum = albumsData.filter(album => album.id === searchTerm);
        mostrarDatos(foundAlbum);
    } else {
        alert("Ingrese un número válido entre 1 y 100.");
    }
}

document.getElementById("btnBuscar").addEventListener("click", buscarPorId);
document.getElementById("btnLimpiar").addEventListener("click", function () {
    cargarDatos();
    document.getElementById("searchInput").value = ""; // Limpiar el campo de búsqueda
});

// Cargar datos al cargar la página
cargarDatos();
