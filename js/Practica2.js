function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            // Limpiar la tabla antes de mostrar nuevos datos
            res.innerHTML = "";

            // Ciclo para mostrar los datos de la tabla
            for (const datos of json) {
                res.innerHTML += `<tr>
                    <td class="columna1">${datos.id}</td>
                    <td class="columna2">${datos.name}</td>
                    <td class="columna3">${datos.username}</td>
                    <td class="columna4">${datos.email}</td>
                    <td class="columna5">${datos.address.street}</td>
                    <td class="columna6">${datos.address.suite}</td>
                    <td class="columna7">${datos.address.city}</td>
                    <td class="columna8">${datos.address.zipcode}</td>
                    <td class="columna9">${datos.address.geo ? datos.address.geo.lat : ''}</td>
                    <td class="columna10">${datos.address.geo ? datos.address.geo.lng : ''}</td>
                    <td class="columna11">${datos.phone}</td>
                    <td class="columna12">${datos.company ? datos.company.name : ''}</td>
                    <td class="columna13">${datos.company ? datos.company.catchPhrase : ''}</td>
                    <td class="columna14">${datos.company ? datos.company.bs : ''}</td>
                </tr>`;
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnCargar").addEventListener('click', cargarDatos);

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
