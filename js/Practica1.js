function cargarDatos(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums"

//realizar funcion de respuesta de peticion
http.onreadystatechange = function(){
    //validar respuesta
    
    if(this.status == 200 && this.readyState ==4){

        let res = document.getElementById('lista');
        const json = JSON.parse(this.responseText);
        //ciclo para mostrar los datos de la tabla
        
        for(const datos of json){
            res.innerHTML+= '<tr> <td class = "columna1">' +datos.userId + '</td>'
            + ' <td class = "columna2">' +datos.id + '</td>'
            + ' <td class = "columna3">' +datos.title + '</td> </tr>'
        }
    
        
    }


}
http.open('GET',url,true);
    http.send();

}

document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click',function(){
    let res = document.getElementById('lista');
    res.innerHTML="";
})