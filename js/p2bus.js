document.getElementById("btnBuscar").addEventListener('click', buscarPorID);
document.getElementById("btnLimpiar").addEventListener('click', limpiarResultado);

function buscarPorID() {
    const idInput = document.getElementById("idInput").value;

    // Validar si el valor está dentro del rango del 1 al 10
    if (!/^[1-9]|10$/.test(idInput)) {
        alert("Ingrese un número del 1 al 10.");
        return;
    }

    const url = `https://jsonplaceholder.typicode.com/users/${idInput}`;

    const http = new XMLHttpRequest();
    
    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            const resultado = document.getElementById('resultadoLista');
            const datos = JSON.parse(this.responseText);

            // Limpiar el resultado antes de mostrar nuevos datos
            resultado.innerHTML = "";

            // Mostrar el resultado en la tabla
            resultado.innerHTML += `<tr>
                <td>${datos.id}</td>
                <td>${datos.name}</td>
                <td>${datos.username}</td>
                <td>${datos.email}</td>
                <td>${datos.address.street}</td>
                <td>${datos.address.suite}</td>
                <td>${datos.address.city}</td>
                <td>${datos.address.zipcode}</td>
                <td>${datos.address.geo ? datos.address.geo.lat : ''}</td>
                <td>${datos.address.geo ? datos.address.geo.lng : ''}</td>
                <td>${datos.phone}</td>
                <td>${datos.company ? datos.company.name : ''}</td>
                <td>${datos.company ? datos.company.catchPhrase : ''}</td>
                <td>${datos.company ? datos.company.bs : ''}</td>
            </tr>`;
        }
    };

    http.open('GET', url, true);
    http.send();
}

function limpiarResultado() {
    document.getElementById('idInput').value = '';
    document.getElementById('resultadoLista').innerHTML = '';
}
